const RANDOM_MAX_VALUE = 100;
const RANDOM_MIN_VALUE = 1;

const localhostURL = "http://localhost:5500"

var jsonPost = {};
var hubUUID;
var nodeUUID;
function GET_request(jsonData, url) {
    $.get(localhostURL + url, {
        //jsonData
    },
    function(dataResponse, statusRequest){
        console.log(dataResponse);
        const obj = JSON.parse(dataResponse);
        hubUUID = obj.hubUUID;
        nodeUUID = obj.nodeUUID;
        console.log(statusRequest);
    });
}

function POST_request(url){

    var temperNum = createRandomNumber();
    var humiNum = createRandomNumber();
    let data = {
        temperature : temperNum,
        humi : humiNum
    };
    jsonPost.hubUUID = hubUUID;
    jsonPost.nodeUUID = nodeUUID;
    // create the json file
    jsonPost.data = data;

    $.post(localhostURL + url,
        jsonPost
    ,
    function(dataResponse, statusRequest){
        console.log(dataResponse);
        console.log(statusRequest);
    });
}

function createRandomNumber(){
    let number = Math.floor((Math.random() * RANDOM_MAX_VALUE) + RANDOM_MIN_VALUE);
    return number;
}


function sendRandomData()
{
    // create the ramdom data and push into the json
    var temperNum = createRandomNumber();
    var humiNum = createRandomNumber();
    let data = {
        temperature : temperNum,
        humi : humiNum
    };

    // create the json file
   //jsonPost.bbg = data;

    //sendAjaxRequest(JSON.stringify(jsonPost));

    //POST_request(JSON.stringify(jsonPost)); 
    GET_request(JSON.stringify(jsonPost), "/register?device=hub");

}

var startBtn = document.getElementById("startBtn");
startBtn.addEventListener("click", sendRandomData, false);

function sendRandomDataNode(){
    console.log(hubUUID);
    GET_request("", "/register?device=node&hubUUID=" + hubUUID);
}

var startBtnNode = document.getElementById("startBtnNode");
startBtnNode.addEventListener("click", sendRandomDataNode, false);

function postData(){


    POST_request("/pData");
}

var post = document.getElementById("postData");
post.addEventListener("click", postData, false);

sendRandomData();


// hubUUID, nodeUUID


// hub uuid 
/*
hubUUID
- number of devices
- status: alive or not
*/



// nodeUUID
/*
- status: alive or not
- function-- control and tracking 
- tracking
    temperature
    humility

- control
    current status
    ON/OFF

- register the message
*/
