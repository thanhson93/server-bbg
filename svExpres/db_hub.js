/*
* db hub
* this file will handle read and write the db of hub
*/

class dbHub{
    constructor(Data_Access_Obj){
        this.Data_Access_Obj = Data_Access_Obj;
    }

    /* create table for main table, store all the hubUUID and total node */
    createTableHubMain(nameDB){
        const SQLQueue = "CREATE TABLE IF NOT EXISTS " + nameDB +" (\
                            id INTEGER PRIMARY KEY AUTOINCREMENT, \
                            hubUUID TEXT,\
                            timestamp INTEGER,\
                            totalNode INTEGER DEFAULT 0)";
        return this.Data_Access_Obj.run(SQLQueue);
    }
    
    /* create table for hub, store all the nodeUUID data */
    createTableHub(nameDB) {
        const SQLQueue = "CREATE TABLE IF NOT EXISTS " + nameDB + "( \
                            id INTEGER PRIMARY KEY AUTOINCREMENT, \
                            nodeUUID TEXT,\
                            timestamp INTEGER )";
        return this.Data_Access_Obj.run(SQLQueue);
    }

    /* insert data for hubUUID in main table */
    insertDataHub(nameDB ,hubUUID, timestamp){
        return this.Data_Access_Obj.run("INSERT INTO "+ nameDB + 
            " (hubUUID, timestamp) VALUES (?, ?)", [hubUUID, timestamp]);
    }

    /* insert data for node UUID hub table */
    insertDataNode(nameDB, nodeUUID, timestamp){
        return this.Data_Access_Obj.run("INSERT INTO " + nameDB +
            " (nodeUUID, timestamp) VALUES (?, ?)", [nodeUUID, timestamp]);
    }

    /* update total node after register node into hub main */
    increaseTotalNode(nameDB, hubUUID){
        return this.Data_Access_Obj.run("UPDATE " + nameDB + 
            " SET totalNode = totalNode + 1 WHERE hubUUID = (?)", [hubUUID]);
    }

    /* close the connection db */
    closeDb(){
        return this.Data_Access_Obj.closeDb();
    }

    /*remaining, not use yet */
    getById(id){
        return this.Data_Access_Obj.run("");
    }

    getById(){
        return this.Data_Access_Obj.run("");
    }
}

module.exports = dbHub;