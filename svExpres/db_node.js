/*
* database node
* handle all the node inside db
*/

class dbNode {
    constructor(Data_Access_Obj){
        this.Data_Access_Obj = Data_Access_Obj;
    }
    /* name db is daytime */
    createTableData(nameDb){
        const SQLQueue = "CREATE TABLE IF NOT EXISTS " + nameDb + " ( \
                            id INTEGER PRIMARY KEY AUTOINCREMENT,\
                            timestamp INTEGER,\
                            temperature INTEGER, \
                            humility INTEGER)";
        return this.Data_Access_Obj.run(SQLQueue);
    }
    createTableSummary(nameDb){
        const SQLQueue = "CREATE TABLE IF NOT EXISTS " + nameDb + " (\
                            id INTEGER PRIMARY KEY AUTOINCREMENT, \
                            date TEXT, \
                            totalRecord INTEGER DEFAULT 1)";
        return this.Data_Access_Obj.run(SQLQueue);
    }

    increaseRecord(nameDb, date){
        return this.Data_Access_Obj.run("UPDATE " + nameDb +
                " SET totalRecord = totalRecord + 1 WHERE date = (?)", [date]);
    }

    insertData(nameDb, timestamp, temperature, humi){
        return this.Data_Access_Obj.run("INSERT INTO " + nameDb + 
                " (timestamp, temperature, humility) VALUES (?, ?, ?)", 
            [timestamp, temperature, humi]);
    }

    insertDataDate(nameDb, date){
        return this.Data_Access_Obj.run("INSERT INTO " + nameDb + 
                " (date) VALUES (?)" , [date]);
    }

    getTotalRecord(nameDb,date){
        return this.Data_Access_Obj.get("SELECT totalRecord FROM " + nameDb + " WHERE date = (?)", [date]);
    }

    closeDb(){
        return this.Data_Access_Obj.closeDb();
    }
}

module.exports = dbNode;
