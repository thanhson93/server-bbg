const express = require("express");
var bodyParser = require('body-parser');
const Generate_UUID = require('uuid/v4');
//import { v4 as Generate_UUID } from 'uuid';
const Promise = require("bluebird");
const fs = require("fs");

/* constant for header message response */
const port = 5500;
const ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
const ACCESS_CONTROL_ALLOW_METHOD = "Access-Control-Allow-Methods";
const ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
const ALL_LINK_REQUEST = "*";
const HTTP_PROTOCOL = "GET,PUT,POST,DELETE";

/* constant for database */
const Application_Data_Acess_Obj = require("./data_access_obj.js");
const dbHub = require("./db_hub.js");
const dbNode = require("./db_node.js");

const dbFolder = "./database/";
const dbHubInfo = "./database/hubInfo.sqlite";
const extensionDb = ".sqlite";
const hubTbMain = "hubInformations";

/* body parsing for express message request */
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/* check the directory is exists or not
 * create the directory
 * */
function createFolderDb(path, UUID){
    var folderName = path + UUID;
    if(!fs.existsSync(folderName)){

        /* create folder for hub */
        fs.mkdirSync(folderName);
    }
}

/* convert the hubUUID into hub table name */
function getNameDbTable(UUID,typeDB){

    var nameDb = UUID.replace(/-/g, "");
    /* the first character of table doesnt allow for number */
    if(typeDB === "hub"){
        nameDb = "h" + nameDb;
    }
    else if(typeDB === "node"){
        nameDb = "n" + nameDb;
    }
    else if(typeDB === "date"){
        nameDb = "d" + nameDb;
    }

    return nameDb;
}

/* register the database for hubUUID */
function handleRegisterHub(hubUUID, timestamp){
    const dataAcccessObjHub = new Application_Data_Acess_Obj(dbHubInfo);
    const hubDb = new dbHub(dataAcccessObjHub);
    const hubTableName = getNameDbTable(hubUUID, "hub");

    /* create folder for hubUUID */
    hubDb.insertDataHub(hubTbMain, hubUUID, timestamp)
        .then(() => {
            /* create table with name of hub */
            hubDb.createTableHub(hubTableName);
        })
        .then(() => {
            /* create folder name with name if hub UUId */
            createFolderDb(dbFolder, hubUUID);
        })
        .then(() =>{
            /*close the db hub after complete register */
            hubDb.closeDb();
        });
}

/* register the nodeUUID into database */
function handleRegisterNode(hubUUID, nodeUUID,timestamp){
    const dataAcccessObjHub = new Application_Data_Acess_Obj(dbHubInfo);
    const hubDb = new dbHub(dataAcccessObjHub);
    var nameHubTb = getNameDbTable(hubUUID, "hub");

    /* increase totoal node */
    hubDb.increaseTotalNode(hubTbMain, hubUUID)
        .then(() =>{
            /* insert the new nodeUUID into hub table */
            hubDb.insertDataNode(nameHubTb, nodeUUID, timestamp);
        })
        .then(() => {
            /* close the hub table */
            hubDb.closeDb();
        });

    /* create the folder for node inside hub folder */
    //createFolderDb(dbFolder + hubUUID + "/", nodeUUID);

    /* create the file db inside */
    var dbFilePathNode = dbFolder + hubUUID + '/' + nodeUUID + extensionDb;
    const dataAcccessObjNode = new Application_Data_Acess_Obj(dbFilePathNode);
    const nodeDb = new dbNode(dataAcccessObjNode);
    nodeDb.createTableSummary(getNameDbTable(nodeUUID, "node"))
        .then(() => {
            nodeDb.closeDb();
        });
}

/*
* handle register request with hub and node
*/
function handleRegisterReq(request)
{
    var timestamp;
    var hubUUID;
    var nodeUUID;
    var msg = {
        status: "success",
        hubUUID: "non",
        nodeUUID: "non"
    };

    if(request.query.device === "node")
    {
        timestamp = new Date();
        msg.hubUUID = request.query.hubUUID;
        msg.nodeUUID = Generate_UUID();
        handleRegisterNode(msg.hubUUID, msg.nodeUUID, timestamp);
    }
    else if(request.query.device === "hub"){
        timestamp = new Date();
        msg.hubUUID = Generate_UUID();
        handleRegisterHub(msg.hubUUID, timestamp);
    }
    else{
        /* the request send wrong message */
        msg.status = "failed";
    }

    return msg;
}

/* convert the timestamp into format yy-mm-dd */
function convertTimestamp(timestamp){
    var time = new Date(timestamp);
    var date = time.getDate();
    var months = time.getMonth();
    var years = time.getFullYear();

    return years + "-" + months + "-" + date; 
}

/* handle the post data from hub */
function handlePostData(req, res){
    var msgRet = {
        status: "success"
    };
    var msg = req.body;
    var timestamp = new Date();
    var dbFilePath = dbFolder + "/" + msg.hubUUID + "/" + msg.nodeUUID + "/";
    var date = "2020-3-12";//convertTimestamp(timestamp);
    const nameTableMainNode = getNameDbTable(msg.nodeUUID, "node");
    const nameTableData = getNameDbTable(date, "date");

    /* create the file db inside */
    var dbFilePathNode = dbFolder + msg.hubUUID + '/' + msg.nodeUUID + extensionDb;

    const dataAcccessObjNode = new Application_Data_Acess_Obj(dbFilePathNode);
    const nodeDb = new dbNode(dataAcccessObjNode);

    nodeDb.createTableSummary(nameTableMainNode)
        .then(() => nodeDb.getTotalRecord(nameTableMainNode, date))
        .then ((data) => {
            console.log(data);
            if(data){
                nodeDb.increaseRecord(nameTableMainNode, date);
            }
            else {
                /* todo: data duplicate*/
                nodeDb.insertDataDate(nameTableMainNode, date);
            }
        })
        .then(() => nodeDb.createTableData(nameTableData))
        .then(() => nodeDb.insertData(nameTableData, timestamp, msg.data.temperature, msg.data.humi))
        .then (() => nodeDb.closeDb)
        .catch((error) => {
            console.log("Error ");
            console.log(JSON.stringify(error));
        });

    return msgRet;
}

/* set header response for access control, cors */
function setHeaderRes(res){
    res.setHeader(ACCESS_CONTROL_ALLOW_ORIGIN, ALL_LINK_REQUEST);
    res.setHeader(ACCESS_CONTROL_ALLOW_METHOD, HTTP_PROTOCOL);
    res.setHeader(ACCESS_CONTROL_ALLOW_HEADERS, true);

    return res;
}

/* register interface */
app.get("/register", function(req, res){
    res = setHeaderRes(res);
    var responseMsg = handleRegisterReq(req);
    res.send(JSON.stringify(responseMsg));
});

/* post data interface */
app.post("/pData", function(req, res) {
    res = setHeaderRes(res);

    var resMsg = handlePostData(req, res);
    res.send(resMsg);
})

/* test get data */
app.get("/testGet", function(req, res){
    res = setHeaderRes(res);
    res.send(req.query);
    console.log("get data message");
});

/* test post data */
app.post("/testPost", function(req, res){
    res = setHeaderRes(res);
    response.send(req.body);
    console.log('post message');
}); 

/* test data in http request */
app.get("/", function(req, res) {
    res.send("hello. the server is running");
})

/* create the hub main database from start the db */
function createDBHubMain(){
   const dataAcccessObjHub = new Application_Data_Acess_Obj(dbHubInfo);
    const hubDb = new dbHub(dataAcccessObjHub);

    /* create the table if not exist */
    hubDb.createTableHubMain(hubTbMain);
    hubDb.closeDb();
}

/* main function to start port and initial the SQLite*/
function main(){
    var server = app.listen(port, function(){
        console.log("server started with port " + port);
    });
    createDBHubMain();
}
// todo: render page to get data

main();
