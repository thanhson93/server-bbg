// data access object

const sqlite3 = require("sqlite3").verbose();
const Promise = require("bluebird");

class Application_Data_Access_Obj{

    /* Create the new db with file Path*/
    constructor(dbFilePath){
        this.db = new sqlite3.Database(dbFilePath, (error) => {
            if(error){
                console.log("Error access to database", error);
                console.log(dbFilePath);
            }
            else{
                //console.log("Connected to database");
            }
        });
    }

    /* close the connection database */
    closeDb(){
        this.db.close((error) => {
            if(error){
                console.log("Error close database", error);
            }
            else {
                //console.log("Closed database");
            }
        });
    }

    /* execute the SQL queue and check the error case */
    run(SQLQueue, params = []){
        return new Promise((resolve, reject) => {
            this.db.run(SQLQueue, params, function(error){
                if(error){
                    console.log("error running SQLqueue: " + SQLQueue);
                    console.log(error);
                    reject(error);
                }
                else {
                    /* todo : check this function and its currently return the last ID */
                    resolve({id: this.lastID});
                }
            })
        });
    }

    /* get the value from SQL db */
    get(SQLQueue, params = []){
        return new Promise ((resolve, reject) => {
            this.db.get(SQLQueue, params, (error, result) =>{
                if(error){
                    console.log("error running SQL: "+ SQLQueue);
                    console.log(error);
                    reject(error);
                }
                else {
                    resolve(result);
                }
            })
        });
    }

    /* Get all the value db */
    all(SQLQueue, params = []){
        return new Promise((resolve, reject) => {
            this.db.all(SQLQueue, params, (error, row) => {
                if(error){
                    console.log("error running SQL: " + SQLQueue);
                    console.log(error);
                    reject(error);
                }
                else{
                    resolve(row);
                }
            })

        });
    }

    /* todo: serialize function SQL */
    serialize(SQLQueueFirst, SQLQueueSec,SQLQueueThird){
        return new Promise ((resolve, reject) => {
            this.db.serialize(() => {
                this.db.run()
            });
        })
    };
}

module.exports = Application_Data_Access_Obj;