// data access object
const sqlite3 = require("sqlite3").verbose();
const Promise = require("bluebird");

class Application_Data_Access_Obj {

	constructor(dbFilePath)
	{
		this.db = new sqlite3.Database(dbFilePath, (error) => {
			if(error) {
				console.log("Error cannot access to database ", error);
			} 
			else {
				console.log('Connected to database');
			}
		})
	}

	/* excute the SQL queue and then return the error */
	run(SQL, params = [])
	{
		return new Promise((resolve, reject) => {
			this.db.run(SQL, params, function (error) {
				if(error){
					console.log('Error running SQL: ' + SQL);
					console.log(error);
					reject(error);
				}
				else {
					resolve({id: this.lastID});
				}
			})
		})
	}

	get(SQL, params = []){
		return new Promise((resolve, reject) => {
			this.db.get(SQL, params, (error, result) =>{
				if(error){
					console.log("Error running SQL: " + SQL);
					console.log(error);
					reject(error);
				}
				else {
					resolve(result);
				}
			})
		})
	}

	all(SQL, params = []){
		return new Promise((resolve, reject) => {
			this.db.all(SQL, params, (error, row) =>{
				if(error){
					console.log("Error running SQL: " + SQL);
					console.log(error);
					reject(error);
				}
				else{
					resolve(row);
				}
			})
		})
	}
}

module.exports = Application_Data_Access_Obj;