// main.js
// https://stackabuse.com/a-sqlite-tutorial-with-node-js/
const Promise = require("bluebird");
const Application_Data_Access_OBJ = require("./data_access_obj.js");
const Project_Repo = require("./project_repo.js");
const Task_Repo = require("./task_repo.js");

function main(){
	const dataAccessObj = new Application_Data_Access_OBJ("./database.sqlite");
	const blogProjectData = {name: "WriteNodejs"};
	const projectRepo = new Project_Repo(dataAccessObj);
	const taskRepo = new Task_Repo(dataAccessObj);
	let projectID;

	projectRepo.createTable()
		.then(() => taskRepo.createTable())
		.then(() => projectRepo.create(blogProjectData.name))
		.then((data) => {
			projectID = data.id;
			const tasks = [
			{
				name : "outline",
				description : "high level overview",
				isComplete : 1,
				projectID
			},
			{
				name : "write",
				description : "write an content",
				isComplete : 0,
				projectID
			}];
			return Promise.all(tasks.map((task) => {
				const {name, description, isComplete, projectID} = task;
				return taskRepo.create(name, description, isComplete, projectID);
			}));
		})
		.then (() => projectRepo.getById(projectID))
		.then ((project) => {
			console.log("Retrived project from database");
			console.log("ProjectID = " + project.id);
			console.log("project name = " + project.name);
			console.log(project);
			return projectRepo.getTasks(project.id);
		})
		.then ((tasks) => {
			console.log("Retrived the project task from db");
			return new Promise((resolve, reject) => {
				tasks.forEach((task) =>{
					console.log("task id = " + task.id);
					console.log("task description = " + task.description);
					console.log("task isComplete = " + task.isComplete);
					console.log("task projID = " + task.projectID);
				})
			})
			resolve("success");
		})
		.catch((error) => {
			console.log("Error ");
			console.log(JSON.stringify(error));
		})

}

main();










