// task repository 

class TaskRepo {
	constructor(Data_Access_OBJ){
		this.Data_Access_OBJ = Data_Access_OBJ;
	};

	createTable(){
		const SQLQueue = "CREATE TABLE IF NOT EXISTS tasks (\
							id INTEGER PRIMARY KEY AUTOINCREMENT,\
							name TEXT,\
							description TEXT,\
							isComplete INTEGER DEFAULT 0,\
							projectID INTEGER,\
							CONSTRAINT tasks_fk_projectId FOREIGN KEY (projectID)\
							REFERENCES projects(id) ON UPDATE CASCADE ON DELETE CASCADE)";
		return this.Data_Access_OBJ.run(SQLQueue);
	};

	create(name, description, isComplete, projectID){
		return this.Data_Access_OBJ.run ("INSERT INTO tasks (name, description, isComplete, projectID) VALUES (?, ?, ?, ?)",
			[name, description, isComplete, projectID]);
	};

	delete(id){
		return this.Data_Access_OBJ.run("DELETE FROM tasks WHERE id = ?", [id]);
	};

	getById(id){
		return this.Data_Access_OBJ.get("SELECT * FROM tasks WHERE id = ?", [id]);
	};

	getByProjectID(projectID){
		return this.Data_Access_OBJ.run("SELECT * FROM tasks WHERE projectID = ?", [projectID]);
	};
}

module.exports = TaskRepo;