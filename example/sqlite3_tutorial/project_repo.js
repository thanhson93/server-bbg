// project repository 

class ProjectRepo{
	constructor(Data_Access_OBJ){
		this.Data_Access_OBJ = Data_Access_OBJ;
	}

	createTable(){
		const SQLQueue = "CREATE TABLE IF NOT EXISTS projects (\
				 				id INTEGER PRIMARY KEY AUTOINCREMENT,\
								name TEXT)";
		return this.Data_Access_OBJ.run(SQLQueue);
	}

	create(name){
		return this.Data_Access_OBJ.run ( "INSERT INTO projects (name) VALUES (?)", [name] );
	}

    delete(id){
        return this.Data_Access_OBJ.run("DELETE FROM projects WHERE id = ?", [id]);
    }

    getById(id){
        return this.Data_Access_OBJ.get("SELECT * FROM projects WHERE id = ?", [id]);
    }

    getAll(){
        return this.Data_Access_OBJ.run("SELECT * FROM projects");
    }

    getTasks(projectID) {
    	return this.Data_Access_OBJ.all("SELECT * FROM tasks WHERE projectID = ?", [projectID]);
    }
};

module.exports = ProjectRepo;
