var http = require('http');
var querystring = require('querystring');

var server = http.createServer().listen(5500);

server.on('request', function (req, res) {
        if (req.method == 'POST') {
                    var body = '';
                }

        req.on('data', function (data) {
                    body += data;
                });

        req.on('end', function () {
                    var post = querystring.parse(body);
                    console.log(post);
                    res.writeHead(200, {
                        'Content-Type': 'text/plain',
                        'Access-Control-Allow-Origin' : '*',
                        'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
                    });
                    res.end('Hello World\n');
                });
});

console.log('Listening on port 3000');
// Dont use this server anymore. this is complicated and complex. its not good. 