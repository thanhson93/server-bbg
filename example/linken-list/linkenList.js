const Node = require(".node.js");

class LinkenList {
    constructor(){
        this.head = null;
        this.size = 0;
    }
    
    add (element){
        // create a new Node
        var node = new Node(element);

        // store current node
        var current;

        // check list and then add the value
        if(this.head == null){
            this.head = node;
        }
        else {
            current = this.head;
            
            // get the latest node, the value next will be null.
            while(current.next){
                current = current.next;
            }
            // add node
            current.next = node;
        }

        this.size ++;
    }

    insertAt(element, index) {
        if((index > 0) && (index > this.size)){
            // check the index value is out size listed
            return false;
        }
        else {
            // create a new node
            var node = new Node(element);
            var current, previous;

            current = this.head;

            if(index == 0){
                node.next= head;
                this.head = node;
            }
            else {
                current = this.head;
                var it = 0;

                while(it < index){
                    it++;
                    previous = current;
                    current = current.next;
                }

                // adding an element
                node.next = current;
                previous.next = node;
            }
            this.size++;
        }
    }

    removeFrom(index){
        if((index > 0) && (index > this.size)){
            return -1;
        }
        else{
            var current, previous, it = 0;
            current = thi.head;

            if(index === 0){
                this.head = current.next;
            }
            else{
                while(it < index){
                    it++;
                    previous = current;
                    current = current.next;
                }

                // remove the element
                previous.next = current.next;
            }
            this.size --;

            // return 
            return current.element;
        }
    }
    
    removeElement(element){
        var current = thi.head;
        var previous = null;

        while(current != null){
            if(current.element == element){
                if(previous == null)
                {
                    this.head = current.next;
                }
                else {
                    previous.next = current.next;
                }
            }
            else{
                this.size --;
                return current.element;    
            }
            previous = current;
            current = current.next;    
        }
        return -1;
    }

    indexOf(element){
        var count = 0;
        var current = this.head;

        while(current != null)
        {
            if(current.element == element){
                return count;
            }
            count ++;
            current = current.element;
        }
        /* not found */
        return -1;
    }

    isEmty(){
        return this.size == 0;
    }

    sizeOfList(){
        return this.size;
    }

    printList(){
        var current = this.head;
        var str = "";
        while(current){
            str += current.element + " ";
            current = current.next;
        }

        console.log(str);
    }
}

// source https://www.geeksforgeeks.org/implementation-linkedlist-javascript/
